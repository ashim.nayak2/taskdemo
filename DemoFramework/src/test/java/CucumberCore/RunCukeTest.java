package CucumberCore;

import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)




@CucumberOptions(
	  features= {
				"src/test/java/CucumberFeatures/Weather.feature",
				
		},
      glue= {"CucumberCore","StepDefinations"},	
      plugin= {"json:target/cucumber/cucumber.json" ,"html:target/cucumber.pretty"},//"junit:target/cucumber/cucumber.xml"

      monochrome=true,
      strict=true,
      dryRun=false
		)

public class RunCukeTest{

}
