package CucumberCore;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


import org.openqa.selenium.WebDriver;

import Utils.DriverFactory;
public class CucumberHook {

	public static WebDriver driver;
	public static Map<String,String> repoMap;
	
	@Before
	public void before(Scenario sc) throws InterruptedException{
		System.out.println("----------------------------------");
		System.out.println("Starting:"+sc.getName());
		System.out.println("----------------------------------");
		System.out.println("Starting A New Driver Instance");
		if(driver==null) {
			System.out.println("CucumberHook:before About to Call Driver");
			DriverFactory df=new DriverFactory();
			driver = df.getDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
			}
		}
	@After
	public void cleanUp()
	{
		if(driver!=null)
		{
			driver.quit();
		}
		driver=null;
	}
		
	}
	

