package Utils;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIMethods {

	
	public String GetWeatherDetails(String city)
	{   
		// Specify the base URL to the RESTful web service
		RestAssured.baseURI = "https://demoqa.com/utilities/weather/city";

		RequestSpecification httpRequest = RestAssured.given();

		Response response = httpRequest.request(Method.GET, "/"+city);

		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		return responseBody;

	}

}
