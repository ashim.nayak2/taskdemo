package Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {
protected WebDriver myDriver;
public DriverFactory() {
	initialize();
}
public void initialize() {
	if (myDriver==null) {
		System.out.println("DriverFactory:initialize method called");
		myDriver= CreateNewDriverInstance();
		
	}
}
private WebDriver CreateNewDriverInstance() {
	
	WebDriver driver=null;
	String browser=new PropertyReader().readProperty("browser");
	System.out.println("DriverFactory:CreateNewDriverInstance method called:browser:"+browser);
	String selenium_grid=new PropertyReader().readProperty("selenium-grid");
	System.out.println("DriverFactory:CreateNewDriverInstance method called:selenium_grid:"+selenium_grid);
	switch(browser) {
	case "chrome":
	//String myOs=
//		System.setProperty("webdriver.chrome.driver", "./Resources/chromedriver.exe");
		Map prefs = new HashMap();
		prefs.put("profile.default_content_settings.cookies", 2);
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions opt=new ChromeOptions();
		opt.addArguments("--disable-notifications");
		opt.setExperimentalOption("prefs", prefs);
		if (selenium_grid.equals("Yes")) {
			System.out.println("About to Start new chrome RemoteWebDriver");
			try {
				driver=new RemoteWebDriver(new URL(""), opt);
			}catch(MalformedURLException e) {
				e.printStackTrace();
			}		
		} else {
			System.out.println("Run Localy");
			driver=new ChromeDriver(opt);
		}
		break;
case "firefox":
	System.setProperty("webdriver.gecko.driver", "./Resources/geckodriver.exe");
	FirefoxOptions fopt=new FirefoxOptions();
	fopt.setCapability("marionette", true);
	driver=new FirefoxDriver(fopt);
	break;
case "IE":	
	//need to implement
	break;
	
	default:
		System.out.println("no browser");
		break;
	
	}
	return driver;
	
}
public WebDriver getDriver() {
	// TODO Auto-generated method stub
	return myDriver;
}
}
