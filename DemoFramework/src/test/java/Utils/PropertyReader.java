package Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

	Properties pro= new Properties();
	FileInputStream fis=null;
	
	public PropertyReader() {
		loadProperties();
	}
	
	private void loadProperties() {
		try {
			fis=new FileInputStream("./Resources/Configuration.properties");
			pro.load(fis);
		}catch(IOException e){
			e.printStackTrace();
			}	
	}

	public String readProperty(String key) {
		loadProperties();
		return pro.getProperty(key);
	}

}
