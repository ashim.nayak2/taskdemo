package ObjectRepository;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utils.PropertyReader;

public class HomePage {
	WebDriver driver;
		public HomePage(WebDriver Edriver) {
			driver=Edriver;
		PageFactory.initElements(Edriver, this);
	    }
		
        String url=new PropertyReader().readProperty("URL");
		
		public void OpenHome() {
			driver.get(url);
			
		}
		
		@FindBy(xpath="//*[@placeholder='Search Location']")
		private WebElement searchBox;
		
		@FindBy(xpath="//*[@data-qa='searchIcon']")
		private WebElement searchIcon;
		
		@FindBy(xpath="//*[@data-qa='tomorrowWeatherCard']")
		private WebElement tomorrowDetails;
		
		public boolean searchBoxPresent() {
			return searchBox.isDisplayed();
			
		}

		public void enterLocation(String txt) {
			searchBox.sendKeys(txt);
			
		}
				
		public void clicksearchIcon() {
			searchIcon.click();
			
		}
		
		public String tomorrowDetailsGet() throws AWTException {
			return tomorrowDetails.getText();
			
		}

}

