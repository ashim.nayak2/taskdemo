package StepDefinations;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import CucumberCore.CucumberHook;
import ObjectRepository.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import Utils.*;
public class HomeDefinations {

APIMethods	api=new APIMethods();
WebDriver driver=CucumberHook.driver;
HomePage hp	= new HomePage(driver);
String location=null;
SoftAssert softAssert = new SoftAssert();

   @Given("^I launch Chrome browser$")
   public void launch_Chrome_browser() throws Throwable {
	   System.out.println("Verify Browesr open");
	   
		
	}
   

	@When("^I open Homepage$")
	public void i_open_Google_Homepage() throws Throwable {
		hp.OpenHome();
	}

	@Then("^I verify that the page displays search text box$")
	public void i_verify_that_the_page_displays_search_text_box() throws Throwable {
	   Assert.assertTrue(hp.searchBoxPresent());
	}
	
	@Then("^enter \"([^\"]*)\"$")
	public void enter_Location(String txt) throws Throwable {
		location=txt;
		hp.enterLocation(txt);
	}
	
	@Then("^I clicked on search icon$")
	public void i_clicked_on_search_icon() throws Throwable {
		hp.clicksearchIcon();
	}

String expectedResult=null;
@Then("^Read Tomorrows Weather$")
public void read_Tomorrows_Weather() throws Throwable {
	expectedResult=hp.tomorrowDetailsGet();
    System.out.println(location+":"+expectedResult);
}

String responseBody=null;
@Then("^Fetch Tomorrows weather using API GET request$")
public void fetch_Tomorrows_weather_using_API_GET_request() throws Throwable {
	 responseBody=api.GetWeatherDetails(location);
}

@Then("^Verify API responce with website values$")
public void verify_API_responce_with_website_values() throws Throwable {
  String[] expectedResults=expectedResult.split("[\r\n]+");
 
  
  
  Object obj=JSONValue.parse(responseBody);    
//creating an object of JSONObject class and casting the object into JSONObject type  
JSONObject jsonObject = (JSONObject) obj;    
//getting values form the JSONObject and casting that values into corresponding types  
String actualTemperature = (String) jsonObject.get("Temperature");        
String actualWeather = (String) jsonObject.get("Weather Description");   
//printing the values   
System.out.println("Actual Temperature: "+actualTemperature);
System.out.println("Expected Temperature: "+expectedResults[2]);

System.out.println("Actual Weather: "+actualWeather);
System.out.println("Expected Weather: "+expectedResults[4]);
  
softAssert.assertEquals(actualTemperature, expectedResults[2]);
softAssert.assertEquals(actualWeather, expectedResults[4]);
softAssert.assertAll();

}


}
