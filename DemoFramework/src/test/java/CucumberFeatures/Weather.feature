Feature: Weather accuracy
  This feature verifies the Weather accuracy

  Scenario Outline: Search any location & verify Tomorrows weather
    Given I launch Chrome browser
    When I open Homepage
    Then I verify that the page displays search text box
    And enter "<Location>"
    Then I clicked on search icon
    And Read Tomorrows Weather
    Then Fetch Tomorrows weather using API GET request
    And Verify API responce with website values

    Examples: 
      | Location  |
      | Kolkata   |
      | Bangalore |